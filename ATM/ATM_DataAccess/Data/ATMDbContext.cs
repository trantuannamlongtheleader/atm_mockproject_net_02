﻿using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Data
{
    public class ATMDbContext : DbContext
    {
        public ATMDbContext()
        {

        }
        public ATMDbContext(DbContextOptions<ATMDbContext> options) : base(options)
        {

        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<ATM> ATMs { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogType> LogTypes { get; set; }
        public DbSet<Money> Moneys { get; set; }
        public DbSet<OverDraftLimit> OverDraftLimits { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<WithDrawLimit> WithDrawLimits { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"server = ADMIN\SQLEXPRESS; database = ATMDb; TrustServerCertificate=True; Integrated security = true");
            //.LogTo(Console.WriteLine, new[] {DbLoggerCategory.Database.Command.Name},LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Setup Primary Key for all models
            
            modelBuilder.Entity<Account>().HasKey(u => u.AccountID);
            modelBuilder.Entity<ATM>().HasKey(u => u.ATMID);
            modelBuilder.Entity<Card>().HasKey(u => u.CardNo);
            modelBuilder.Entity<Config>().HasKey(u => u.ConfigID);
            modelBuilder.Entity<Customer>().HasKey(u => u.CustID);
            modelBuilder.Entity<Log>().HasKey(u => u.LogID);
            modelBuilder.Entity<LogType>().HasKey(u => u.LogTypeID);
            modelBuilder.Entity<Money>().HasKey(u => u.MoneyID);
            modelBuilder.Entity<OverDraftLimit>().HasKey(u => u.ODID);
            modelBuilder.Entity<Stock>().HasKey(u => u.StockID);
            modelBuilder.Entity<WithDrawLimit>().HasKey(u => u.WDID);


            // Setup Relationship (1:n),(1:1) between those models

            modelBuilder.Entity<Account>().HasOne(u => u.Customer).WithMany
                (u => u.Accounts).HasForeignKey(u => u.CustID);
            modelBuilder.Entity<Account>().HasOne(u => u.OverDraftLimit).WithMany
                (u => u.Accounts).HasForeignKey(u => u.ODID);
            modelBuilder.Entity<Account>().HasOne(u => u.WithDrawLimit).WithMany
                (u => u.Accounts).HasForeignKey(u => u.WDID);
            
            modelBuilder.Entity<Card>().HasOne(u => u.Account).WithMany
                (u => u.Cards).HasForeignKey(u => u.AccountID);
            
            modelBuilder.Entity<Log>().HasOne(u => u.Card).WithMany
                (u => u.Logs).HasForeignKey(u => u.CardNo);
            modelBuilder.Entity<Log>().HasOne(u => u.LogType).WithMany
                (u => u.Logs).HasForeignKey(u => u.LogTypeID);
            modelBuilder.Entity<Log>().HasOne(u => u.ATM).WithMany
                (u => u.Logs).HasForeignKey(u => u.ATMID);
            
            modelBuilder.Entity<Stock>().HasOne(u => u.ATM).WithMany
                (u => u.Stocks).HasForeignKey(u => u.ATMID);
            modelBuilder.Entity<Stock>().HasOne(u => u.Money).WithMany
                (u => u.Stocks).HasForeignKey(u => u.MoneyID);

            // Setup requirement for properties of all models

            modelBuilder.Entity<Customer>().Property(c => c.Name).HasMaxLength(100).HasColumnType("nvarchar");
            modelBuilder.Entity<Customer>().Property(c => c.Phone).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Address).HasMaxLength(200);

            modelBuilder.Entity<Account>().Property(c => c.AccountNo).HasMaxLength(50);

            modelBuilder.Entity<Card>().Property(c => c.CardNo).HasMaxLength(16);
            modelBuilder.Entity<Card>().Property(c => c.Status).HasMaxLength(30);
            modelBuilder.Entity<Card>().Property(c => c.PIN).HasMaxLength(6);

            modelBuilder.Entity<ATM>().Property(c => c.Branch).HasMaxLength(50);
            modelBuilder.Entity<ATM>().Property(c => c.Address).HasMaxLength(100);

            modelBuilder.Entity<Money>().Property(c => c.Address).HasMaxLength(100);

            modelBuilder.Entity<LogType>().Property(c => c.Description).HasMaxLength(100);

            modelBuilder.Entity<Log>().Property(c => c.CardNo).HasMaxLength(16);
            modelBuilder.Entity<Log>().Property(c => c.Details).HasMaxLength(100);







            var customers = new Customer[]
           {
                new Customer{CustID=1,Name="Edward NewGate", Phone="123444555", Email="edward@gmail.com", Address="New World"},
                new Customer{CustID=2,Name="Binh Le", Phone="789444555", Email="binh9x@gmail.com", Address="Hanoi"},
                new Customer{CustID=3,Name="An Trinh", Phone="222444555", Email="an12@gmail.com", Address="Vinh"},
                new Customer{CustID=4,Name="Tung Nguyen", Phone="123444232", Email="tung8x@gmail.com", Address="Hoian"},
                new Customer{CustID=5,Name="Luffy Monkey D", Phone="123444676", Email="luffy@gmail.com", Address="East Blue"},
                new Customer{CustID=6,Name="Ronoa Zoro", Phone="123444090", Email="zoro123@gmail.com", Address="East Blue"},
                new Customer{CustID=7,Name="Uzumaki Naruto", Phone="091345090", Email="narutohokage@gmail.com", Address="Tokyo"},
                new Customer{CustID=8,Name="Uchiha Sasuke", Phone="091232121", Email="sasuke2k@gmail.com", Address="Tokyo"},
                new Customer{CustID=9,Name="Joe Biden", Phone="090012020", Email="bidenUSA@gmail.com", Address="Newyork"},
                new Customer{CustID=10,Name="Volodymyr Zelensky", Phone="090898343", Email="zelenUSA@gmail.com", Address="Kiev"},
                new Customer{CustID=11,Name="Vladimir Putin", Phone="0001001999", Email="putinZOV@gmail.com", Address="Moskva"},
                new Customer{CustID=12,Name="Vu Van Nghiep", Phone="036763999", Email="nghiepVL9x@gmail.com", Address="Namdinh"}
                
           };
            modelBuilder.Entity<Customer>().HasData(customers);

            var ods = new OverDraftLimit[]
            {
                new OverDraftLimit{ODID=1,Value=490.5m},
                new OverDraftLimit{ODID=2,Value=123.5m},
                new OverDraftLimit{ODID=3,Value=567.5m},
                new OverDraftLimit{ODID=4,Value=890.3m},
                new OverDraftLimit{ODID=5,Value=1000.2m},
                new OverDraftLimit{ODID=6,Value=50.9m},
                new OverDraftLimit{ODID=7,Value=88.6m},
                new OverDraftLimit{ODID=8,Value=609.9m},
                new OverDraftLimit{ODID=9,Value=377.7m},
                new OverDraftLimit{ODID=10,Value=2300.9m},
                new OverDraftLimit{ODID=11,Value=55100.5m},
                new OverDraftLimit{ODID=12,Value=120.1m}
                
            };
            modelBuilder.Entity<OverDraftLimit>().HasData(ods);

            var wds = new WithDrawLimit[]
            {
                new WithDrawLimit{WDID=1,Value=190.5m},
                new WithDrawLimit{WDID=2,Value=90.5m},
                new WithDrawLimit{WDID=3,Value=26.5m},
                new WithDrawLimit{WDID=4,Value=55.8m},
                new WithDrawLimit{WDID=5,Value=222.2m},
                new WithDrawLimit{WDID=6,Value=780.9m},
                new WithDrawLimit{WDID=7,Value=20.0m},
                new WithDrawLimit{WDID=8,Value=44.9m},
                new WithDrawLimit{WDID=9,Value=100.5m},
                new WithDrawLimit{WDID=10,Value=1060.4m},
                new WithDrawLimit{WDID=11,Value=10505.8m},
                new WithDrawLimit{WDID=12,Value=99.9m},
                
            };
            modelBuilder.Entity<WithDrawLimit>().HasData(wds);



            var accounts = new Account[]
            {
                new Account{AccountID=1,CustID=1,AccountNo="123456",ODID=1,WDID=1,Balance=500.5m},
                new Account{AccountID=2,CustID=1,AccountNo="123455",ODID=1,WDID=2,Balance=900.5m},
                new Account{AccountID=3,CustID=2,AccountNo="222111",ODID=1,WDID=2,Balance=700.5m},
                new Account{AccountID=4,CustID=2,AccountNo="222112",ODID=2,WDID=2,Balance=600.5m},
                new Account{AccountID=5,CustID=3,AccountNo="311001",ODID=2,WDID=3,Balance=500.5m},
                new Account{AccountID=6,CustID=3,AccountNo="311002",ODID=3,WDID=3,Balance=888.5m},
                new Account{AccountID=7,CustID=4,AccountNo="400001",ODID=4,WDID=4,Balance=111.5m},
                new Account{AccountID=8,CustID=4,AccountNo="400102",ODID=5,WDID=5,Balance=80.5m},
                new Account{AccountID=9,CustID=5,AccountNo="500011",ODID=5,WDID=6,Balance=50.5m},
                new Account{AccountID=10,CustID=5,AccountNo="500102",ODID=6,WDID=6,Balance=99.5m},
                new Account{AccountID=11,CustID=6,AccountNo="600100",ODID=6,WDID=7,Balance=340.5m},
                new Account{AccountID=12,CustID=7,AccountNo="700100",ODID=7,WDID=8,Balance=222.5m},
                new Account{AccountID=13,CustID=8,AccountNo="800100",ODID=8,WDID=8,Balance=550.5m},
                new Account{AccountID=14,CustID=9,AccountNo="900100",ODID=9,WDID=9,Balance=696.5m},
                new Account{AccountID=15,CustID=10,AccountNo="101000",ODID=9,WDID=9,Balance=886.5m},
                new Account{AccountID=16,CustID=10,AccountNo="101001",ODID=10,WDID=9,Balance=1000.5m},
                new Account{AccountID=17,CustID=11,AccountNo="110011",ODID=10,WDID=10,Balance=9900.5m},
                new Account{AccountID=18,CustID=11,AccountNo="110012",ODID=11,WDID=11,Balance=5123.5m},
                new Account{AccountID=19,CustID=11,AccountNo="110013",ODID=10,WDID=12,Balance=12055.5m},
                new Account{AccountID=20,CustID=12,AccountNo="120011",ODID=12,WDID=12,Balance=234.5m}
            };
            modelBuilder.Entity<Account>().HasData(accounts);


            var cards = new Card[]
            {
                new Card{CardNo="111000",PIN="000000",Status="normal",StartDate=new DateTime(2023,1,25),ExpiredDate=new DateTime(2028,1,25),AccountID=1,Attempt=1},
                new Card{CardNo="111001",PIN="000001",Status="normal",StartDate=new DateTime(2023,2,25),ExpiredDate=new DateTime(2028,1,25),AccountID=1,Attempt=2},
                new Card{CardNo="111002",PIN="000002",Status="normal",StartDate=new DateTime(2023,3,25),ExpiredDate=new DateTime(2028,1,25),AccountID=2,Attempt=3},
                new Card{CardNo="111003",PIN="000003",Status="normal",StartDate=new DateTime(2023,4,25),ExpiredDate=new DateTime(2028,1,25),AccountID=2,Attempt=0},
                new Card{CardNo="111004",PIN="000004",Status="normal",StartDate=new DateTime(2023,1,25),ExpiredDate=new DateTime(2028,1,25),AccountID=3,Attempt=2},
                new Card{CardNo="111005",PIN="000005",Status="normal",StartDate=new DateTime(2023,1,25),ExpiredDate=new DateTime(2028,1,25),AccountID=3,Attempt=3},
                new Card{CardNo="111006",PIN="000006",Status="normal",StartDate=new DateTime(2023,1,25),ExpiredDate=new DateTime(2028,1,25),AccountID=3,Attempt=2},
                new Card{CardNo="111007",PIN="000007",Status="normal",StartDate=new DateTime(2023,2,25),ExpiredDate=new DateTime(2028,1,25),AccountID=4,Attempt=1},
                new Card{CardNo="111008",PIN="000008",Status="normal",StartDate=new DateTime(2023,2,25),ExpiredDate=new DateTime(2028,1,25),AccountID=5,Attempt=1},
                new Card{CardNo="111009",PIN="000009",Status="normal",StartDate=new DateTime(2023,3,24),ExpiredDate=new DateTime(2028,1,25),AccountID=6,Attempt=0},
                new Card{CardNo="111010",PIN="000010",Status="normal",StartDate=new DateTime(2023,3,24),ExpiredDate=new DateTime(2028,1,25),AccountID=7,Attempt=0},
                new Card{CardNo="111011",PIN="000011",Status="normal",StartDate=new DateTime(2023,1,20),ExpiredDate=new DateTime(2028,1,25),AccountID=7,Attempt=0},
                new Card{CardNo="111012",PIN="000012",Status="normal",StartDate=new DateTime(2023,1,11),ExpiredDate=new DateTime(2028,1,25),AccountID=8,Attempt=0},
                new Card{CardNo="111013",PIN="000013",Status="normal",StartDate=new DateTime(2023,2,15),ExpiredDate=new DateTime(2028,1,25),AccountID=9,Attempt=0},
                new Card{CardNo="111014",PIN="000014",Status="normal",StartDate=new DateTime(2023,4,10),ExpiredDate=new DateTime(2028,1,25),AccountID=10,Attempt=0},
                new Card{CardNo="111015",PIN="000015",Status="normal",StartDate=new DateTime(2023,5,9),ExpiredDate=new DateTime(2028,1,25),AccountID=10,Attempt=1},
                new Card{CardNo="111016",PIN="000016",Status="normal",StartDate=new DateTime(2023,7,6),ExpiredDate=new DateTime(2028,1,25),AccountID=11,Attempt=1},
                new Card{CardNo="111017",PIN="000017",Status="normal",StartDate=new DateTime(2023,8,8),ExpiredDate=new DateTime(2028,1,25),AccountID=11,Attempt=1},
                new Card{CardNo="111018",PIN="000018",Status="normal",StartDate=new DateTime(2023,8,17),ExpiredDate=new DateTime(2028,1,25),AccountID=12,Attempt=1},
                new Card{CardNo="111019",PIN="000019",Status="normal",StartDate=new DateTime(2023,8,10),ExpiredDate=new DateTime(2028,1,25),AccountID=13,Attempt=1},
                new Card{CardNo="111020",PIN="000020",Status="normal",StartDate=new DateTime(2023,8,5),ExpiredDate=new DateTime(2028,1,25),AccountID=14,Attempt=2},
                new Card{CardNo="111021",PIN="000021",Status="normal",StartDate=new DateTime(2023,3,5),ExpiredDate=new DateTime(2028,1,25),AccountID=15,Attempt=0},
                new Card{CardNo="111022",PIN="000022",Status="normal",StartDate=new DateTime(2023,4,15),ExpiredDate=new DateTime(2028,1,25),AccountID=16,Attempt=0}
            };
            modelBuilder.Entity<Card>().HasData(cards);


            var atms = new ATM[]
            {
                new ATM{ATMID=1, Branch="Thanh Xuan", Address="467 Nguyen Trai Street, Thanh Xuan District, Hanoi"},
                new ATM{ATMID=2, Branch="My Đinh", Address="The Manor Landmark Tower, Me Tri Street, Tu Liem District, Hanoi"},
                new ATM{ATMID=3, Branch="KeangNam", Address="Keangnam Landmark Tower, E6 Pham Hung Street, Cau Giay District, Hanoi"},
                new ATM{ATMID=4, Branch="Thang Long", Address="181 Nguyen Luong Bang, Dong Đa District, Hanoi"},
                new ATM{ATMID=5, Branch="Ha Noi", Address="15 Dao Duy Tu,Hoan Kiem District, Hanoi"},
                new ATM{ATMID=6, Branch="Trang An", Address="Floor 1, T4- Times City, 458 Minh Khai ,Hai Ba Trung District, Hanoi"},
                new ATM{ATMID=7, Branch="Ha Tay", Address="Floor 1,Ct2 Tower,Ngo Thi Nham Street, Ha Dong District, Hanoi"},
                new ATM{ATMID=8, Branch="Xa La", Address="2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi"}
            };
            modelBuilder.Entity<ATM>().HasData(atms);

            var moneys = new Money[]
            {
                new Money{MoneyID=1,MoneyValue=99.4m,Address="467 Nguyen Trai Street, Thanh Xuan District, Hanoi"},
                new Money{MoneyID=2,MoneyValue=120.4m,Address="467 Nguyen Trai Street, Thanh Xuan District, Hanoi"},
                new Money{MoneyID=3,MoneyValue=240.4m,Address="15 Dao Duy Tu,Hoan Kiem District, Hanoi"},
                new Money{MoneyID=4,MoneyValue=450.4m,Address="15 Dao Duy Tu,Hoan Kiem District, Hanoi"},
                new Money{MoneyID=5,MoneyValue=670.4m,Address="15 Dao Duy Tu,Hoan Kiem District, Hanoi"},
                new Money{MoneyID=6,MoneyValue=880.4m,Address="Floor 1, T4- Times City, 458 Minh Khai ,Hai Ba Trung District, Hanoi"},
                new Money{MoneyID=7,MoneyValue=560.4m,Address="2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi"},
                new Money{MoneyID=8,MoneyValue=2600.4m,Address="2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi"},
                new Money{MoneyID=9,MoneyValue=1230.5m,Address="181 Nguyen Luong Bang, Dong Đa District, Hanoi"},
                new Money{MoneyID=10,MoneyValue=333.7m,Address="Keangnam Landmark Tower, E6 Pham Hung Street, Cau Giay District, Hanoi"},
                new Money{MoneyID=11,MoneyValue=1567.7m,Address="Floor 1,Ct2 Tower,Ngo Thi Nham Street, Ha Dong District, Hanoi"},
                new Money{MoneyID=12,MoneyValue=6789.7m,Address="The Manor Landmark Tower, Me Tri Street, Tu Liem District, Hanoi"}
            };
            modelBuilder.Entity<Money>().HasData(moneys);

            var stocks = new Stock[]
            {
                new Stock{StockID=1,MoneyID=1,ATMID=1,Quantity=10},
                new Stock{StockID=2,MoneyID=1,ATMID=2,Quantity=6},
                new Stock{StockID=3,MoneyID=1,ATMID=3,Quantity=7},
                new Stock{StockID=4,MoneyID=2,ATMID=1,Quantity=3},
                new Stock{StockID=5,MoneyID=2,ATMID=3,Quantity=50},
                new Stock{StockID=6,MoneyID=3,ATMID=4,Quantity=14},
                new Stock{StockID=7,MoneyID=4,ATMID=5,Quantity=15},
                new Stock{StockID=8,MoneyID=5,ATMID=5,Quantity=22},
                new Stock{StockID=9,MoneyID=6,ATMID=7,Quantity=27},
                new Stock{StockID=10,MoneyID=7,ATMID=8,Quantity=30},
                new Stock{StockID=11,MoneyID=7,ATMID=1,Quantity=36},
                new Stock{StockID=12,MoneyID=7,ATMID=7,Quantity=57},
                new Stock{StockID=13,MoneyID=8,ATMID=8,Quantity=88},
                new Stock{StockID=14,MoneyID=9,ATMID=2,Quantity=49},
                new Stock{StockID=15,MoneyID=9,ATMID=4,Quantity=36},
                new Stock{StockID=16,MoneyID=10,ATMID=1,Quantity=68},
                new Stock{StockID=17,MoneyID=10,ATMID=2,Quantity=77},
                new Stock{StockID=18,MoneyID=10,ATMID=3,Quantity=99},
                new Stock{StockID=19,MoneyID=11,ATMID=5,Quantity=85},
                new Stock{StockID=20,MoneyID=11,ATMID=6,Quantity=87},
                new Stock{StockID=21,MoneyID=11,ATMID=7,Quantity=10},
                new Stock{StockID=22,MoneyID=12,ATMID=8,Quantity=20}
            };
            modelBuilder.Entity<Stock>().HasData(stocks);
        }
    }
}
