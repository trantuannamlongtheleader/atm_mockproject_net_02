﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace ATM_DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class CreateDbATM : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ATMs",
                columns: table => new
                {
                    ATMID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Branch = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATMs", x => x.ATMID);
                });

            migrationBuilder.CreateTable(
                name: "Configs",
                columns: table => new
                {
                    ConfigID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MinWithDraw = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MaxWithDraw = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    NumPerPage = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configs", x => x.ConfigID);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustID);
                });

            migrationBuilder.CreateTable(
                name: "LogTypes",
                columns: table => new
                {
                    LogTypeID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogTypes", x => x.LogTypeID);
                });

            migrationBuilder.CreateTable(
                name: "Moneys",
                columns: table => new
                {
                    MoneyID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MoneyValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Moneys", x => x.MoneyID);
                });

            migrationBuilder.CreateTable(
                name: "OverDraftLimits",
                columns: table => new
                {
                    ODID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OverDraftLimits", x => x.ODID);
                });

            migrationBuilder.CreateTable(
                name: "WithDrawLimits",
                columns: table => new
                {
                    WDID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WithDrawLimits", x => x.WDID);
                });

            migrationBuilder.CreateTable(
                name: "Stocks",
                columns: table => new
                {
                    StockID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MoneyID = table.Column<int>(type: "int", nullable: false),
                    ATMID = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stocks", x => x.StockID);
                    table.ForeignKey(
                        name: "FK_Stocks_ATMs_ATMID",
                        column: x => x.ATMID,
                        principalTable: "ATMs",
                        principalColumn: "ATMID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Stocks_Moneys_MoneyID",
                        column: x => x.MoneyID,
                        principalTable: "Moneys",
                        principalColumn: "MoneyID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    AccountID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountNo = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ODID = table.Column<int>(type: "int", nullable: false),
                    WDID = table.Column<int>(type: "int", nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CustID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.AccountID);
                    table.ForeignKey(
                        name: "FK_Accounts_Customers_CustID",
                        column: x => x.CustID,
                        principalTable: "Customers",
                        principalColumn: "CustID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounts_OverDraftLimits_ODID",
                        column: x => x.ODID,
                        principalTable: "OverDraftLimits",
                        principalColumn: "ODID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounts_WithDrawLimits_WDID",
                        column: x => x.WDID,
                        principalTable: "WithDrawLimits",
                        principalColumn: "WDID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cards",
                columns: table => new
                {
                    CardNo = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: false),
                    Status = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    PIN = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpiredDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Attempt = table.Column<int>(type: "int", nullable: false),
                    AccountID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cards", x => x.CardNo);
                    table.ForeignKey(
                        name: "FK_Cards_Accounts_AccountID",
                        column: x => x.AccountID,
                        principalTable: "Accounts",
                        principalColumn: "AccountID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    LogID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogTypeID = table.Column<int>(type: "int", nullable: false),
                    ATMID = table.Column<int>(type: "int", nullable: false),
                    CardNo = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: false),
                    LogDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Details = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.LogID);
                    table.ForeignKey(
                        name: "FK_Logs_ATMs_ATMID",
                        column: x => x.ATMID,
                        principalTable: "ATMs",
                        principalColumn: "ATMID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Logs_Cards_CardNo",
                        column: x => x.CardNo,
                        principalTable: "Cards",
                        principalColumn: "CardNo",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Logs_LogTypes_LogTypeID",
                        column: x => x.LogTypeID,
                        principalTable: "LogTypes",
                        principalColumn: "LogTypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ATMs",
                columns: new[] { "ATMID", "Address", "Branch" },
                values: new object[,]
                {
                    { 1, "467 Nguyen Trai Street, Thanh Xuan District, Hanoi", "Thanh Xuan" },
                    { 2, "The Manor Landmark Tower, Me Tri Street, Tu Liem District, Hanoi", "My Đinh" },
                    { 3, "Keangnam Landmark Tower, E6 Pham Hung Street, Cau Giay District, Hanoi", "KeangNam" },
                    { 4, "181 Nguyen Luong Bang, Dong Đa District, Hanoi", "Thang Long" },
                    { 5, "15 Dao Duy Tu,Hoan Kiem District, Hanoi", "Ha Noi" },
                    { 6, "Floor 1, T4- Times City, 458 Minh Khai ,Hai Ba Trung District, Hanoi", "Trang An" },
                    { 7, "Floor 1,Ct2 Tower,Ngo Thi Nham Street, Ha Dong District, Hanoi", "Ha Tay" },
                    { 8, "2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi", "Xa La" }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustID", "Address", "Email", "Name", "Phone" },
                values: new object[,]
                {
                    { 1, "New World", "edward@gmail.com", "Edward NewGate", "123444555" },
                    { 2, "Hanoi", "binh9x@gmail.com", "Binh Le", "789444555" },
                    { 3, "Vinh", "an12@gmail.com", "An Trinh", "222444555" },
                    { 4, "Hoian", "tung8x@gmail.com", "Tung Nguyen", "123444232" },
                    { 5, "East Blue", "luffy@gmail.com", "Luffy Monkey D", "123444676" },
                    { 6, "East Blue", "zoro123@gmail.com", "Ronoa Zoro", "123444090" },
                    { 7, "Tokyo", "narutohokage@gmail.com", "Uzumaki Naruto", "091345090" },
                    { 8, "Tokyo", "sasuke2k@gmail.com", "Uchiha Sasuke", "091232121" },
                    { 9, "Newyork", "bidenUSA@gmail.com", "Joe Biden", "090012020" },
                    { 10, "Kiev", "zelenUSA@gmail.com", "Volodymyr Zelensky", "090898343" },
                    { 11, "Moskva", "putinZOV@gmail.com", "Vladimir Putin", "0001001999" },
                    { 12, "Namdinh", "nghiepVL9x@gmail.com", "Vu Van Nghiep", "036763999" }
                });

            migrationBuilder.InsertData(
                table: "Moneys",
                columns: new[] { "MoneyID", "Address", "MoneyValue" },
                values: new object[,]
                {
                    { 1, "467 Nguyen Trai Street, Thanh Xuan District, Hanoi", 99.4m },
                    { 2, "467 Nguyen Trai Street, Thanh Xuan District, Hanoi", 120.4m },
                    { 3, "15 Dao Duy Tu,Hoan Kiem District, Hanoi", 240.4m },
                    { 4, "15 Dao Duy Tu,Hoan Kiem District, Hanoi", 450.4m },
                    { 5, "15 Dao Duy Tu,Hoan Kiem District, Hanoi", 670.4m },
                    { 6, "Floor 1, T4- Times City, 458 Minh Khai ,Hai Ba Trung District, Hanoi", 880.4m },
                    { 7, "2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi", 560.4m },
                    { 8, "2nd 8th Manor Xa La Urban Area, Ha Dong, Hanoi", 2600.4m },
                    { 9, "181 Nguyen Luong Bang, Dong Đa District, Hanoi", 1230.5m },
                    { 10, "Keangnam Landmark Tower, E6 Pham Hung Street, Cau Giay District, Hanoi", 333.7m },
                    { 11, "Floor 1,Ct2 Tower,Ngo Thi Nham Street, Ha Dong District, Hanoi", 1567.7m },
                    { 12, "The Manor Landmark Tower, Me Tri Street, Tu Liem District, Hanoi", 6789.7m }
                });

            migrationBuilder.InsertData(
                table: "OverDraftLimits",
                columns: new[] { "ODID", "Value" },
                values: new object[,]
                {
                    { 1, 490.5m },
                    { 2, 123.5m },
                    { 3, 567.5m },
                    { 4, 890.3m },
                    { 5, 1000.2m },
                    { 6, 50.9m },
                    { 7, 88.6m },
                    { 8, 609.9m },
                    { 9, 377.7m },
                    { 10, 2300.9m },
                    { 11, 55100.5m },
                    { 12, 120.1m }
                });

            migrationBuilder.InsertData(
                table: "WithDrawLimits",
                columns: new[] { "WDID", "Value" },
                values: new object[,]
                {
                    { 1, 190.5m },
                    { 2, 90.5m },
                    { 3, 26.5m },
                    { 4, 55.8m },
                    { 5, 222.2m },
                    { 6, 780.9m },
                    { 7, 20.0m },
                    { 8, 44.9m },
                    { 9, 100.5m },
                    { 10, 1060.4m },
                    { 11, 10505.8m },
                    { 12, 99.9m }
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "AccountID", "AccountNo", "Balance", "CustID", "ODID", "WDID" },
                values: new object[,]
                {
                    { 1, "123456", 500.5m, 1, 1, 1 },
                    { 2, "123455", 900.5m, 1, 1, 2 },
                    { 3, "222111", 700.5m, 2, 1, 2 },
                    { 4, "222112", 600.5m, 2, 2, 2 },
                    { 5, "311001", 500.5m, 3, 2, 3 },
                    { 6, "311002", 888.5m, 3, 3, 3 },
                    { 7, "400001", 111.5m, 4, 4, 4 },
                    { 8, "400102", 80.5m, 4, 5, 5 },
                    { 9, "500011", 50.5m, 5, 5, 6 },
                    { 10, "500102", 99.5m, 5, 6, 6 },
                    { 11, "600100", 340.5m, 6, 6, 7 },
                    { 12, "700100", 222.5m, 7, 7, 8 },
                    { 13, "800100", 550.5m, 8, 8, 8 },
                    { 14, "900100", 696.5m, 9, 9, 9 },
                    { 15, "101000", 886.5m, 10, 9, 9 },
                    { 16, "101001", 1000.5m, 10, 10, 9 },
                    { 17, "110011", 9900.5m, 11, 10, 10 },
                    { 18, "110012", 5123.5m, 11, 11, 11 },
                    { 19, "110013", 12055.5m, 11, 10, 12 },
                    { 20, "120011", 234.5m, 12, 12, 12 }
                });

            migrationBuilder.InsertData(
                table: "Stocks",
                columns: new[] { "StockID", "ATMID", "MoneyID", "Quantity" },
                values: new object[,]
                {
                    { 1, 1, 1, 10 },
                    { 2, 2, 1, 6 },
                    { 3, 3, 1, 7 },
                    { 4, 1, 2, 3 },
                    { 5, 3, 2, 50 },
                    { 6, 4, 3, 14 },
                    { 7, 5, 4, 15 },
                    { 8, 5, 5, 22 },
                    { 9, 7, 6, 27 },
                    { 10, 8, 7, 30 },
                    { 11, 1, 7, 36 },
                    { 12, 7, 7, 57 },
                    { 13, 8, 8, 88 },
                    { 14, 2, 9, 49 },
                    { 15, 4, 9, 36 },
                    { 16, 1, 10, 68 },
                    { 17, 2, 10, 77 },
                    { 18, 3, 10, 99 },
                    { 19, 5, 11, 85 },
                    { 20, 6, 11, 87 },
                    { 21, 7, 11, 10 },
                    { 22, 8, 12, 20 }
                });

            migrationBuilder.InsertData(
                table: "Cards",
                columns: new[] { "CardNo", "AccountID", "Attempt", "ExpiredDate", "PIN", "StartDate", "Status" },
                values: new object[,]
                {
                    { "111000", 1, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000000", new DateTime(2023, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111001", 1, 2, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000001", new DateTime(2023, 2, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111002", 2, 3, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000002", new DateTime(2023, 3, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111003", 2, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000003", new DateTime(2023, 4, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111004", 3, 2, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000004", new DateTime(2023, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111005", 3, 3, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000005", new DateTime(2023, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111006", 3, 2, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000006", new DateTime(2023, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111007", 4, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000007", new DateTime(2023, 2, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111008", 5, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000008", new DateTime(2023, 2, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111009", 6, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000009", new DateTime(2023, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111010", 7, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000010", new DateTime(2023, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111011", 7, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000011", new DateTime(2023, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111012", 8, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000012", new DateTime(2023, 1, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111013", 9, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000013", new DateTime(2023, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111014", 10, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000014", new DateTime(2023, 4, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111015", 10, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000015", new DateTime(2023, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111016", 11, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000016", new DateTime(2023, 7, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111017", 11, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000017", new DateTime(2023, 8, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111018", 12, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000018", new DateTime(2023, 8, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111019", 13, 1, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000019", new DateTime(2023, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111020", 14, 2, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000020", new DateTime(2023, 8, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111021", 15, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000021", new DateTime(2023, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" },
                    { "111022", 16, 0, new DateTime(2028, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "000022", new DateTime(2023, 4, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "normal" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustID",
                table: "Accounts",
                column: "CustID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_ODID",
                table: "Accounts",
                column: "ODID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_WDID",
                table: "Accounts",
                column: "WDID");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_AccountID",
                table: "Cards",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_ATMID",
                table: "Logs",
                column: "ATMID");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_CardNo",
                table: "Logs",
                column: "CardNo");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_LogTypeID",
                table: "Logs",
                column: "LogTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_ATMID",
                table: "Stocks",
                column: "ATMID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_MoneyID",
                table: "Stocks",
                column: "MoneyID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Configs");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Stocks");

            migrationBuilder.DropTable(
                name: "Cards");

            migrationBuilder.DropTable(
                name: "LogTypes");

            migrationBuilder.DropTable(
                name: "ATMs");

            migrationBuilder.DropTable(
                name: "Moneys");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "OverDraftLimits");

            migrationBuilder.DropTable(
                name: "WithDrawLimits");
        }
    }
}
