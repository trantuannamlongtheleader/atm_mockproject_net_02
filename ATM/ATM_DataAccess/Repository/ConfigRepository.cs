﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class ConfigRepository : IGenericRepository<Config>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Config> configs;

        public ConfigRepository(ATMDbContext db)
        {
            _db = db;
            configs = db.Set<Config>();
        }
        public void Add(Config t)
        {
            configs.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Config t)
        {
            configs.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            configs.Remove(configs.Find(id));
            _db.SaveChanges();
        }

        public Config Find(int id)
        {
            return configs.Find(id);
        }

        public IList<Config> GetAll()
        {
            return configs.ToList();
        }

        public void Update(Config t)
        {
            configs.Update(t);
            _db.SaveChanges();
        }
    }
}
