﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class LogRepository : IGenericRepository<Log>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Log> logs;

        public LogRepository(ATMDbContext db)
        {
            _db = db;
            logs = db.Set<Log>();
        }
        public void Add(Log t)
        {
            logs.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Log t)
        {
            logs.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            logs.Remove(logs.Find(id));
            _db.SaveChanges();
        }

        public Log Find(int id)
        {
            return logs.Find(id);
        }

        public IList<Log> GetAll()
        {
            return logs.ToList();
        }

        public void Update(Log t)
        {
            logs.Update(t);
            _db.SaveChanges();
        }
    }
}
