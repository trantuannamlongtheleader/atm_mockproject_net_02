﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class AccountRepository : IGenericRepository<Account>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Account> accounts;

        public AccountRepository(ATMDbContext db)
        {
            _db = db;
            accounts = db.Set<Account>();
        }
        public void Add(Account t)
        {
            accounts.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Account t)
        {
            accounts.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            accounts.Remove(accounts.Find(id));
            _db.SaveChanges();
        }

        public Account Find(int id)
        {
           return accounts.Find(id);
        }

        public IList<Account> GetAll()
        {
            return accounts.ToList();
        }

        public void Update(Account t)
        {
            accounts.Update(t);
            _db.SaveChanges();
        }
    }
}
