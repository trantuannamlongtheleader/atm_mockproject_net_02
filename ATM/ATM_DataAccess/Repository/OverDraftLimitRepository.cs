﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class OverDraftLimitRepository : IGenericRepository<OverDraftLimit>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<OverDraftLimit> ods;

        public OverDraftLimitRepository(ATMDbContext db)
        {
            _db = db;
            ods = db.Set<OverDraftLimit>();
        }
        public void Add(OverDraftLimit t)
        {
            ods.Add(t);
            _db.SaveChanges();
        }

        public void Delete(OverDraftLimit t)
        {
            ods.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            ods.Remove(ods.Find(id));
            _db.SaveChanges();
        }

        public OverDraftLimit Find(int id)
        {
            return ods.Find(id);
        }

        public IList<OverDraftLimit> GetAll()
        {
            return ods.ToList();
        }

        public void Update(OverDraftLimit t)
        {
            ods.Update(t);
            _db.SaveChanges();
        }
    }
}
