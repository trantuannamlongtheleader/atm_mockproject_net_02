﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class WithDrawLimitRepository : IGenericRepository<WithDrawLimit>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<WithDrawLimit> wds;

        public WithDrawLimitRepository(ATMDbContext db)
        {
            _db = db;
            wds = db.Set<WithDrawLimit>();
        }
        public void Add(WithDrawLimit t)
        {
            wds.Add(t);
            _db.SaveChanges();
        }

        public void Delete(WithDrawLimit t)
        {
            wds.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            wds.Remove(wds.Find(id));
            _db.SaveChanges();
        }

        public WithDrawLimit Find(int id)
        {
            return wds.Find(id);
        }

        public IList<WithDrawLimit> GetAll()
        {
            return wds.ToList();
        }

        public void Update(WithDrawLimit t)
        {
            wds.Update(t);
            _db.SaveChanges();
        }
    }
}
