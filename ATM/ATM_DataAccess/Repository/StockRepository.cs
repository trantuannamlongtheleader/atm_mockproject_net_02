﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class StockRepository : IGenericRepository<Stock>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Stock> stocks;

        public StockRepository(ATMDbContext db)
        {
            _db = db;
            stocks = db.Set<Stock>();
        }
        public void Add(Stock t)
        {
            stocks.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Stock t)
        {
            stocks.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            stocks.Remove(stocks.Find(id));
            _db.SaveChanges();
        }

        public Stock Find(int id)
        {
            return stocks.Find(id);
        }

        public IList<Stock> GetAll()
        {
            return stocks.ToList();
        }

        public void Update(Stock t)
        {
            stocks.Update(t);
            _db.SaveChanges();
        }
    }
}
