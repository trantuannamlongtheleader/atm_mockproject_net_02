﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class ATMRepository : IGenericRepository<ATM>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<ATM> atms;

        public ATMRepository(ATMDbContext db)
        {
            _db = db;
            atms = db.Set<ATM>();
        }
        public void Add(ATM t)
        {
            atms.Add(t);
            _db.SaveChanges();
        }

        public void Delete(ATM t)
        {
            atms.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            atms.Remove(atms.Find(id));
            _db.SaveChanges();
        }

        public ATM Find(int id)
        {
            return atms.Find(id);
        }

        public IList<ATM> GetAll()
        {
            return atms.ToList();
        }

        public void Update(ATM t)
        {
            atms.Update(t);
            _db.SaveChanges();
        }
    }
}
