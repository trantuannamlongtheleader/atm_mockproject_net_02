﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class MoneyRepository : IGenericRepository<Money>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Money> moneys;

        public MoneyRepository(ATMDbContext db)
        {
            _db = db;
            moneys = db.Set<Money>();
        }
        public void Add(Money t)
        {
            moneys.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Money t)
        {
            moneys.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            moneys.Remove(moneys.Find(id));
            _db.SaveChanges();
        }

        public Money Find(int id)
        {
            return moneys.Find(id);
        }

        public IList<Money> GetAll()
        {
            return moneys.ToList();
        }

        public void Update(Money t)
        {
            moneys.Update(t);
            _db.SaveChanges();
        }
    }
}
