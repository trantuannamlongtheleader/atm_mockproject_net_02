﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository.IRepository
{
    public interface IGenericRepository<T>
    {
        T Find(int id);
        void Add(T t);
        void Update(T t);
        void Delete(T t);
        void Delete(int id);
        IList<T> GetAll();
    }
}
