﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class CustomerRepository : IGenericRepository<Customer>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Customer> customers;

        public CustomerRepository(ATMDbContext db)
        {
            _db = db;
            customers = db.Set<Customer>();
        }
        public void Add(Customer t)
        {
            customers.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Customer t)
        {
            customers.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            customers.Remove(customers.Find(id));
            _db.SaveChanges();
        }

        public Customer Find(int id)
        {
            return customers.Find(id);
        }

        public IList<Customer> GetAll()
        {
            return customers.ToList();
        }

        public void Update(Customer t)
        {
            customers.Update(t);
            _db.SaveChanges();
        }
    }
}
