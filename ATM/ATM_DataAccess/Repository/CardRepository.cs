﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class CardRepository : IGenericRepository<Card>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<Card> cards;

        public CardRepository(ATMDbContext db)
        {
            _db = db;
            cards = db.Set<Card>();
        }
        public void Add(Card t)
        {
            cards.Add(t);
            _db.SaveChanges();
        }

        public void Delete(Card t)
        {
            cards.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            cards.Remove(cards.Find(id));
            _db.SaveChanges();
        }

        public Card Find(int id)
        {
            return cards.Find(id);
        }

        public IList<Card> GetAll()
        {
            return cards.ToList();
        }

        public void Update(Card t)
        {
            cards.Update(t);
            _db.SaveChanges();
        }
    }
}
