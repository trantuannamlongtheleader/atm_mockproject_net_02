﻿using ATM_DataAccess.Data;
using ATM_DataAccess.Repository.IRepository;
using ATM_Model.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_DataAccess.Repository
{
    public class LogTypeRepository : IGenericRepository<LogType>
    {
        private readonly ATMDbContext _db;
        private readonly DbSet<LogType> logtypes;

        public LogTypeRepository(ATMDbContext db)
        {
            _db = db;
            logtypes = db.Set<LogType>();
        }
        public void Add(LogType t)
        {
            logtypes.Add(t);
            _db.SaveChanges();
        }

        public void Delete(LogType t)
        {
            logtypes.Remove(t);
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            logtypes.Remove(logtypes.Find(id));
            _db.SaveChanges();
        }

        public LogType Find(int id)
        {
            return logtypes.Find(id);
        }

        public IList<LogType> GetAll()
        {
            return logtypes.ToList();
        }

        public void Update(LogType t)
        {
            logtypes.Update(t);
            _db.SaveChanges();
        }
    }
}
