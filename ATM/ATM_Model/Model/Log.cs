﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Log
    {
        public int LogID { get; set; }
        public int LogTypeID { get; set; }
        public LogType? LogType { get; set; }
        public int ATMID { get; set; }
        public ATM? ATM { get; set; }
        public string CardNo { get; set; }
        public Card? Card { get; set; }
        public DateTime LogDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }
        public string Details { get; set; }
    }
}
