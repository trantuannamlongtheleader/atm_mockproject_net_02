﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Account
    {
        public int AccountID { get; set; }
        public string AccountNo { get; set; }
        public int ODID { get; set; }
        public OverDraftLimit? OverDraftLimit { get; set; }
        public int WDID { get; set; }
        public WithDrawLimit? WithDrawLimit { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Balance { get; set; }
        public int CustID { get; set; }
        public Customer? Customer { get; set; }

        public IList<Card>? Cards { get; set; }
    }
}
