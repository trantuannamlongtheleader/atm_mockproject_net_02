﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class LogType
    {
        public int LogTypeID { get; set; }
        public string Description { get; set; }

        public IList<Log>? Logs { get; set; }
    }
}
