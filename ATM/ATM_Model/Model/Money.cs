﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Money
    {
        public int MoneyID { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MoneyValue { get; set; }
        public string Address { get; set; }
        public IList<Stock>? Stocks { get; set; }
    }
}
