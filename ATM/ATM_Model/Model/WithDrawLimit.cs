﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class WithDrawLimit
    {
        public int WDID { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Value { get; set; }

        public IList<Account>? Accounts { get; set; }
    }
}
