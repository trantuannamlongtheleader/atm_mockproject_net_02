﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class ATM
    {
        public int ATMID { get; set; }
        public string Branch { get; set; }
        public string Address { get; set; }

        public IList<Log>? Logs { get; set; }
        public IList<Stock>? Stocks { get; set; }
    }   
}
