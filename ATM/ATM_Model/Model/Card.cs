﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Card
    {
        public string CardNo { get; set; }
        public string Status { get; set; }
        public string PIN { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Attempt { get; set; }
        public int AccountID { get; set; }
        public Account? Account { get; set; }

        public IList<Log>? Logs { get; set; }
    }
}
