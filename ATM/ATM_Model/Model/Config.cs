﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Config
    {
        public int ConfigID { get; set; }
        public DateTime DateModified { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MinWithDraw { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MaxWithDraw { get; set; }
        public int NumPerPage { get; set; }
    }
}
