﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_Model.Model
{
    public class Stock
    {
        public int StockID { get; set; }
        public int MoneyID { get; set; }
        public Money? Money { get; set; }
        public int ATMID { get; set; }
        public ATM? ATM { get; set; }
        public int Quantity { get; set; }
    }
}
